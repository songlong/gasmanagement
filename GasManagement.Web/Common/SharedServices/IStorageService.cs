﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace GasManagement.Service.SharedServices.ServiceInterfaces
{
    public interface IStorageService
    {
        void SaveFile(Guid guid, IFormFile file);

        Task SaveFileAsync(Guid guid, IFormFile file);

        Guid SaveFile(IFormFile file);

        Task<Guid> SaveFileAsync(IFormFile file);

        byte[] Read(string guid);

        Task<byte[]> ReadAsync(string guid);

        Stream ReadStream(string guid);

        FileStream ReadFileStream(string guid);
    }
}
