﻿using System;
using System.IO;
using System.Threading.Tasks;
using GasManagement.Service.SharedServices.ServiceInterfaces;
using GasManagement.Web.Common.Const;
using GasManagement.Web.Common.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace GasManagement.Service.SharedServices.ServiceImplementations
{
    public class StorageService : IStorageService
    {
        public StorageService(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        private readonly IHostingEnvironment _hostingEnvironment;

        private string StorageFilePath => Path.Combine(_hostingEnvironment.WebRootPath, StoragePathConst.Storage);

        public void SaveFile(Guid guid, IFormFile file)
        {
            var fullFilePath = GetFilePath(StorageFilePath, guid);

            using (var fileStream = new FileStream(fullFilePath, FileMode.Create))
            {
                file.CopyTo(fileStream);
            }
        }
        public async Task SaveFileAsync(Guid guid, IFormFile file)
        {
            var fullFilePath = GetFilePath(StorageFilePath, guid);

            using (var fileStream = new FileStream(fullFilePath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
        }

        public Guid SaveFile(IFormFile file)
        {
            var guid = Guid.NewGuid();

            var fullFilePath = GetFilePath(StorageFilePath, guid);

            using (var fileStream = new FileStream(fullFilePath, FileMode.Create))
            {
                file.CopyTo(fileStream);
            }

            return guid;
        }
        public async Task<Guid> SaveFileAsync(IFormFile file)
        {
            if (file == null)
            {
                return Guid.Empty;
            }
            var guid = Guid.NewGuid();

            var fullFilePath = GetFilePath(StorageFilePath, string.Concat(guid, ".", file.FileName.Split('.')?.LastOrDefault()));

            using (var fileStream = new FileStream(fullFilePath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }

            return guid;
        }

        public byte[] Read(string guid)
        {
            var fullFilePath = GetFilePath(StorageFilePath, guid);

            return File.Exists(fullFilePath) ? File.ReadAllBytes(fullFilePath) : null;
        }

        public async Task<byte[]> ReadAsync(string guid)
        {
            var fullFilePath = GetFilePath(StorageFilePath, guid);

            return File.Exists(fullFilePath) ? await File.ReadAllBytesAsync(fullFilePath) : null;
        }

        public Stream ReadStream(string guid)
        {
            var fullFilePath = GetFilePath(StorageFilePath, guid);
            return File.Exists(fullFilePath) ? new FileStream(fullFilePath, FileMode.Open, FileAccess.Read, FileShare.Read) : null;
        }
        public FileStream ReadFileStream(string guid)
        {
            var fullFilePath = GetFilePath(StorageFilePath, guid);
            return File.Exists(fullFilePath) ? new FileStream(fullFilePath, FileMode.Open, FileAccess.Read, FileShare.Read) : null;
        }

        private string GetFilePath(string storageFilePath, Guid guid)
        {
            return GetFilePath(storageFilePath, guid.ToString());
        }

        private string GetFilePath(string storageFilePath, string guid)
        {
            if (storageFilePath.IsNullOrEmpty())
            {
                throw new Exception("Storage File Path Has Not Been Configured!");
            }

            if (!Directory.Exists(storageFilePath))
            {
                Directory.CreateDirectory(storageFilePath);
            }

            return Path.Combine(storageFilePath, guid.ToString());
        }
    }
}
