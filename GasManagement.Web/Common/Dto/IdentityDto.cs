﻿namespace GasManagement.Web.Dto
{
    public class IdentityDto
    {
        public int Id { get; set; }
    }
}
