﻿using GasManagement.Web.Models;
using System.Collections.Generic;

namespace GasManagement.Web.Dto
{
    public class GasDetailDto
    {
        public int GasId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public decimal ImportPrice { get; set; }
        public decimal WholesalePrice { get; set; }
        public decimal RetailPrice { get; set; }
        public int GasShellAmount { get; set; }
        public int GasWaterAmount { get; set; }
        public List<HistoryImportGas> ListHistoryImportGas { get; set; }
    }
}
