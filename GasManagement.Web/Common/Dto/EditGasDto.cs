﻿namespace GasManagement.Web.Dto
{
    public class EditGasDto : CreateGasDto
    {
        public int GasId { get; set; }
    }
}
