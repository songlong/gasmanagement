﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GasManagement.Web.Common.Extensions
{
    public static class ICollectionExtension
    {
        public static bool HasAny<T>(this ICollection<T> collection)
        {
            return collection != null && collection.Any();
        }

        public static bool HasAny<T>(this ICollection<T> collection, Func<T, bool> predicate)
        {
            return collection != null && collection.Any(predicate);
        }
    }
}
