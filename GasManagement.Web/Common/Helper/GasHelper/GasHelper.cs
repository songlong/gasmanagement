﻿using GasManagement.Web.Common.Const;
using GasManagement.Web.Dto;
using GasManagement.Web.Models;
using System.IO;

namespace GasManagement.Web.Helper.GasHelper
{
    public static class GasHelper
    {
        public static GasDetailDto ToDto(this Gas gas)
        {
            return gas == null
                ? null
                : new GasDetailDto
                {
                    Name = gas.Name,
                    Description = gas.Description,
                    GasId = gas.Id,
                    Image = Path.Combine(StoragePathConst.Storage, gas.Image ?? string.Empty),
                    ImportPrice = gas.GasPrices.ImportPrice,
                    RetailPrice = gas.GasPrices.RetailPrice,
                    WholesalePrice = gas.GasPrices.WholesalePrice,
                    GasShellAmount = gas.GasStorage?.GasShellAmount ?? 0,
                    GasWaterAmount = gas.GasStorage?.GasWaterAmount ?? 0,
                    ListHistoryImportGas = gas.HistoryImportGas 
                };
        } 
    }
}
