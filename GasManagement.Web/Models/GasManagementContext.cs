﻿using Microsoft.EntityFrameworkCore;

namespace GasManagement.Web.Models
{
    public class GasManagementContext : DbContext
    {
        public GasManagementContext(DbContextOptions<GasManagementContext> dbContextOptions) : base(dbContextOptions)
    {
    }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=LAPTOP-QA3NS24Q\SQLEXPRESS;Database=Test;Trusted_Connection=True;");
        }

        public DbSet<Gas> Gas { get; set; }
        public DbSet<GasPrice> GasPrices { get; set; }
        public DbSet<GasStorage> GasStorage { get; set; }
        public DbSet<HistoryImportGas> HistoryImportGas { get; set; }
    }
}