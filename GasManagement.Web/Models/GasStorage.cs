﻿using GasManagement.Web.Infra.EntityInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GasManagement.Web.Models
{
    public class GasStorage : IEntity

    {
        [Column("GasStorageId")]
        public int Id { get; set; }
        public int GasId { get; set; }
        public int GasWaterAmount { get; set; }
        public int GasShellAmount { get; set; }
        [ForeignKey("GasId")]
        public Gas Gas { get; set; }
    }
}
