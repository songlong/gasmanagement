﻿using GasManagement.Web.Infra.EntityInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GasManagement.Web.Models
{
    public class HistoryImportGas : IEntity
    {
        [Column("HistoryImportGasId")]
        public int Id { get; set; }
        public DateTime DateImport { get; set; }
        public int GasId { get; set; }
        public int NumberGasWaterImport { get; set; }
        public int NumberGasShellExport { get; set; }
        public DateTime PaydayMoney { get; set; }
        [ForeignKey("GasId")]
        public Gas Gas { get; set; }
    }
}
