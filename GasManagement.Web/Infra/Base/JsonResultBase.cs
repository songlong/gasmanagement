﻿namespace GasManagement.Web.Infra.Base
{
    public class JsonResultBase
    {
        public bool IsSuccess { get; set; }
    }

    public class JsonSuccessRequest : JsonResultBase
    {
        public JsonSuccessRequest()
        {
            IsSuccess = true;
        }

        public JsonSuccessRequest(object _entity)
        {
            Entity = _entity;
            IsSuccess = true;
        }
        public object Entity { get; set; }
    }

    public class JsonFailRequest : JsonResultBase
    {
        public JsonFailRequest()
        {
            IsSuccess = false;
        }
    }
}