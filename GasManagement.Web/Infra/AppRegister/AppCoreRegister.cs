﻿using GasManagement.Service.SharedServices.ServiceImplementations;
using GasManagement.Service.SharedServices.ServiceInterfaces;
using GasManagement.Web.Infra.Middleware;
using GasManagement.Web.Infra.UoW;
using Microsoft.Extensions.DependencyInjection;

namespace GasManagement.Web.Infra.AppRegister
{
    public static class AppCoreRegister
    {
        public static void Register(IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddMvcCore(mvc => mvc.Filters.Add(typeof(GlobalFilter)));

            services.AddScoped<IStorageService, StorageService>();
        }
    }
}
