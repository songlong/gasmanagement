﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;

namespace GasManagement.Web.Infra.Middleware
{
    public class GlobalFilter : IExceptionFilter
    {
        private readonly ILogger<GlobalFilter> _globalLogger;

        public GlobalFilter( /*RequestDelegate next, */ ILogger<GlobalFilter> globalLogger)
        {
            _globalLogger = globalLogger;
        }

        public void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            if (exception == null)
            {
                return;
            }
            _globalLogger.LogError(new EventId(exception.HResult), exception, exception.Message);
            var exceptionMessage = string.Concat(exception.Message, Environment.NewLine, exception.InnerException);
            // case error is a business error

            context.ExceptionHandled = true;
        }
    }
}