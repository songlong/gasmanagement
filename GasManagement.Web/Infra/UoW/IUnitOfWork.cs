﻿using System;
using System.Threading.Tasks;
using GasManagement.Web.Infra.EntityInterface;
using Microsoft.EntityFrameworkCore;

namespace GasManagement.Web.Infra.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        DbContext DbContext { get; }

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity;

        Task<int> SaveChangesAsync();
    }
}