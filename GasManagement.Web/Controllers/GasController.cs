﻿using GasManagement.Service.SharedServices.ServiceInterfaces;
using GasManagement.Web.Common.Const;
using GasManagement.Web.Dto;
using GasManagement.Web.Helper.GasHelper;
using GasManagement.Web.Infra.Base;
using GasManagement.Web.Infra.UoW;
using GasManagement.Web.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GasManagement.Web.Controllers
{
    public class GasController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStorageService _storageService;

        public GasController(IUnitOfWork unitOfWork, IStorageService storageService)
        {
            _unitOfWork = unitOfWork;
            _storageService = storageService;
        }

        private IRepository<Gas> _gasRepository => _unitOfWork.GetRepository<Gas>();
        private IRepository<GasPrice> _gasPriceRepository => _unitOfWork.GetRepository<GasPrice>();
        private IRepository<GasStorage> _gasStorageRepository => _unitOfWork.GetRepository<GasStorage>();

        public IActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GetListGas()
        {
            var gasList = await _gasRepository
                .GetAll()
                .Include(x => x.GasPrices)
                .Include(x => x.GasStorage)
                .Include(x => x.HistoryImportGas)
                .Where(x => !x.IsDeleted)
                .ToListAsync();

            var a = gasList.Select(x => x.ToDto());

            return new JsonResult(new JsonSuccessRequest(gasList.Select(x => x.ToDto())));
        }

        [HttpPost]
        public async Task<JsonResult> CreateGas(CreateGasDto gas, IFormFile file)
        {
            var guid = await _storageService.SaveFileAsync(file);
            var extension = file?.FileName?.Split('.')?.LastOrDefault() ?? string.Empty;
            var newGas = new Gas
            {
                Name = gas.Name,
                Description = gas.Description,
                Image = file != null ? string.Concat(guid.ToString(), ".", extension) : null
            };

            var newGasGromDb = await _gasRepository.InsertAsync(newGas);
            await _unitOfWork.SaveChangesAsync();

            var newGasPrice = new GasPrice
            {
                GasId = newGasGromDb.Id,
                ImportPrice = gas.ImportPrice,
                WholesalePrice = gas.WholesalePrice,
                RetailPrice = gas.RetailPrice,
                Date = DateTime.Now
            };
            await _gasPriceRepository.InsertAsync(newGasPrice);

            var newGasStorage = new GasStorage
            {
                GasId = newGasGromDb.Id,
                GasShellAmount = gas.GasShellAmount,
                GasWaterAmount = gas.GasWaterAmount
            };
            await _gasStorageRepository.InsertAsync(newGasStorage);
            await _unitOfWork.SaveChangesAsync();
            return new JsonResult(new JsonSuccessRequest());
        }

        //chua can action nay
        //[HttpPost]
        //public async Task<JsonResult> EditGas(EditGasDto gas)
        //{
        //    var gasFromDb = await _gasRepository.GetAsync(gas.GasId);

        //    if (gasFromDb == null)
        //    {
        //        return new JsonResult(new JsonFailRequest());
        //    }

        //    var gasPriceFromDb = await _gasPriceRepository.GetFirstOrDefaultAsync(x => x.GasId == gas.GasId);

        //    if (gasPriceFromDb == null)
        //    {
        //        return new JsonResult(new JsonFailRequest());
        //    }

        //    gasFromDb.Description = gas.Description;
        //    gasFromDb.Image = gas.Image;
        //    await _gasRepository.UpdateAsync(gasFromDb);

        //    gasPriceFromDb.ImportPrice = gas.ImportPrice;
        //    gasPriceFromDb.WholesalePrice = gas.WholesalePrice;
        //    gasPriceFromDb.RetailPrice = gas.RetailPrice;
        //    await _gasPriceRepository.UpdateAsync(gasPriceFromDb);

        //    await _unitOfWork.SaveChangesAsync();
        //    return new JsonResult(new JsonSuccessRequest());
        //}

        [HttpPost]
        public async Task<JsonResult> DeleteGas(IdentityDto identity)
        {
            await _gasRepository.DeleteAsync(identity.Id);
            await _unitOfWork.SaveChangesAsync();
            return new JsonResult(new JsonSuccessRequest());
        }
    }
}
