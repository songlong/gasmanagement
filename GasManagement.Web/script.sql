USE [Test]
GO
/****** Object:  Table [dbo].[CashAdvance]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CashAdvance](
	[CashAdvanceId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[DateReceivingMoney] [date] NOT NULL,
	[NumberMoney] [money] NOT NULL,
 CONSTRAINT [PK_CashAdvance] PRIMARY KEY CLUSTERED 
(
	[CashAdvanceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Telephone] [varchar](11) NULL,
	[Note] [nvarchar](max) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[AliasName] [nvarchar](50) NOT NULL,
	[Birthday] [date] NULL,
	[NumberIdentity] [varchar](15) NULL,
	[Image] [varchar](max) NULL,
	[HomeTown] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Expenditure]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Expenditure](
	[ExpenditureId] [int] IDENTITY(1,1) NOT NULL,
	[PaydayMoney] [datetime] NOT NULL,
	[LExpenditureId] [int] NOT NULL,
	[Money] [money] NOT NULL,
	[Note] [nvarchar](max) NULL,
 CONSTRAINT [PK_Expenditure] PRIMARY KEY CLUSTERED 
(
	[ExpenditureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Gas]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Gas](
	[GasId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Image] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Gas] PRIMARY KEY CLUSTERED 
(
	[GasId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GasCustomer]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GasCustomer](
	[GasCustomerId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[GasId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_GasCustomer] PRIMARY KEY CLUSTERED 
(
	[GasCustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GasPrices]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GasPrices](
	[GasPriceId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[GasId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ImportPrice] [money] NOT NULL,
	[RetailPrice] [money] NOT NULL,
	[WholesalePrice] [money] NOT NULL,
 CONSTRAINT [PK_GasPrices] PRIMARY KEY CLUSTERED 
(
	[GasPriceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GasStorage]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GasStorage](
	[GasStorageId] [int] IDENTITY(1,1) NOT NULL,
	[GasId] [int] NULL,
	[NumberGasWater] [int] NULL,
	[NumberGasShell] [int] NULL,
 CONSTRAINT [PK_GasStorage] PRIMARY KEY CLUSTERED 
(
	[GasStorageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Gift]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Gift](
	[GiftId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Image] [varchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Gift] PRIMARY KEY CLUSTERED 
(
	[GiftId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GiftPrice]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiftPrice](
	[GiftPriceId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[GiftId] [int] NOT NULL,
	[ImportPrice] [money] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_GiftPrice] PRIMARY KEY CLUSTERED 
(
	[GiftPriceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GiftStorage]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiftStorage](
	[GiftStorageId] [int] IDENTITY(1,1) NOT NULL,
	[GiftId] [int] NOT NULL,
	[NumberGift] [int] NOT NULL,
 CONSTRAINT [PK_GiftStorage] PRIMARY KEY CLUSTERED 
(
	[GiftStorageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryImportGas]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryImportGas](
	[HistoryImportGasId] [int] IDENTITY(1,1) NOT NULL,
	[DateImport] [datetime] NOT NULL,
	[GasId] [int] NOT NULL,
	[NumberGasWaterImport] [int] NULL,
	[NumberGasShellExport] [int] NULL,
	[PaydayMoney] [datetime] NULL,
 CONSTRAINT [PK_HistoryImportGas] PRIMARY KEY CLUSTERED 
(
	[HistoryImportGasId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryImportGift]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryImportGift](
	[HistoryImportGiftId] [int] NOT NULL,
	[DateImport] [datetime] NOT NULL,
	[GiftId] [int] NOT NULL,
	[Amount] [int] NOT NULL,
	[PaydayMoney] [datetime] NULL,
 CONSTRAINT [PK_HistoryImportGift] PRIMARY KEY CLUSTERED 
(
	[HistoryImportGiftId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryImportProduct]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryImportProduct](
	[HistoryImportProductId] [int] IDENTITY(1,1) NOT NULL,
	[DateImport] [datetime] NULL,
	[ProductId] [int] NULL,
	[Amount] [int] NULL,
	[PaydayMoney] [datetime] NULL,
 CONSTRAINT [PK_HistoryImportProduct] PRIMARY KEY CLUSTERED 
(
	[HistoryImportProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LExpenditure]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LExpenditure](
	[LExpenditureId] [int] IDENTITY(1,1) NOT NULL,
	[TypeExpenditureId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_LExpenditure] PRIMARY KEY CLUSTERED 
(
	[LExpenditureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[OrderId] [int] NULL,
	[Date] [datetime] NULL,
	[CustomerId] [int] NULL,
	[GasId] [int] NULL,
	[ProductId] [int] NULL,
	[GiftId] [int] NULL,
	[EmployeeId] [int] NULL,
	[Note] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Image] [varchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductPrice]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductPrice](
	[ProductPriceId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NOT NULL,
	[ProductId] [int] NOT NULL,
	[ImportPrice] [money] NOT NULL,
	[RetailPrice] [money] NOT NULL,
	[WholesalePrice] [money] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_ProductPrice] PRIMARY KEY CLUSTERED 
(
	[ProductPriceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductStorage]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductStorage](
	[ProductStorageId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[Amount] [int] NOT NULL,
 CONSTRAINT [PK_ProductStorage] PRIMARY KEY CLUSTERED 
(
	[ProductStorageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Salary]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salary](
	[SalaryId] [int] IDENTITY(1,1) NOT NULL,
	[MonthSalary] [date] NULL,
	[EmployeeId] [int] NULL,
	[AmountGas] [int] NULL,
	[SalaryUnitId] [int] NULL,
	[BonusMoney] [money] NOT NULL,
	[AdvanceMoney] [money] NOT NULL,
	[Payday] [datetime] NOT NULL,
 CONSTRAINT [PK_Salary] PRIMARY KEY CLUSTERED 
(
	[SalaryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SalaryUnit]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalaryUnit](
	[SalaryUnitId] [int] IDENTITY(1,1) NOT NULL,
	[DateStart] [date] NOT NULL,
	[SalaryUnit] [money] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_SalaryUnit] PRIMARY KEY CLUSTERED 
(
	[SalaryUnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TypeExpenditure]    Script Date: 6/30/2019 2:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeExpenditure](
	[TypeExpenditureId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TypeExpenditure] PRIMARY KEY CLUSTERED 
(
	[TypeExpenditureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GasPrices] ADD  DEFAULT ((0.0)) FOR [ImportPrice]
GO
ALTER TABLE [dbo].[GasPrices] ADD  DEFAULT ((0.0)) FOR [RetailPrice]
GO
ALTER TABLE [dbo].[GasPrices] ADD  DEFAULT ((0.0)) FOR [WholesalePrice]
GO
ALTER TABLE [dbo].[CashAdvance]  WITH CHECK ADD  CONSTRAINT [FK_CashAdvance_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO
ALTER TABLE [dbo].[CashAdvance] CHECK CONSTRAINT [FK_CashAdvance_Employee]
GO
ALTER TABLE [dbo].[Expenditure]  WITH CHECK ADD  CONSTRAINT [FK_Expenditure_LExpenditure] FOREIGN KEY([LExpenditureId])
REFERENCES [dbo].[LExpenditure] ([LExpenditureId])
GO
ALTER TABLE [dbo].[Expenditure] CHECK CONSTRAINT [FK_Expenditure_LExpenditure]
GO
ALTER TABLE [dbo].[GasCustomer]  WITH CHECK ADD  CONSTRAINT [FK_GasCustomer_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[GasCustomer] CHECK CONSTRAINT [FK_GasCustomer_Customer]
GO
ALTER TABLE [dbo].[GasCustomer]  WITH CHECK ADD  CONSTRAINT [FK_GasCustomer_Gas] FOREIGN KEY([GasId])
REFERENCES [dbo].[Gas] ([GasId])
GO
ALTER TABLE [dbo].[GasCustomer] CHECK CONSTRAINT [FK_GasCustomer_Gas]
GO
ALTER TABLE [dbo].[GasPrices]  WITH CHECK ADD  CONSTRAINT [FK_GasPrices_Gas_GasId] FOREIGN KEY([GasId])
REFERENCES [dbo].[Gas] ([GasId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[GasPrices] CHECK CONSTRAINT [FK_GasPrices_Gas_GasId]
GO
ALTER TABLE [dbo].[GasStorage]  WITH CHECK ADD  CONSTRAINT [FK_GasStorage_Gas] FOREIGN KEY([GasId])
REFERENCES [dbo].[Gas] ([GasId])
GO
ALTER TABLE [dbo].[GasStorage] CHECK CONSTRAINT [FK_GasStorage_Gas]
GO
ALTER TABLE [dbo].[GiftPrice]  WITH CHECK ADD  CONSTRAINT [FK_GiftPrice_Gift] FOREIGN KEY([GiftId])
REFERENCES [dbo].[Gift] ([GiftId])
GO
ALTER TABLE [dbo].[GiftPrice] CHECK CONSTRAINT [FK_GiftPrice_Gift]
GO
ALTER TABLE [dbo].[GiftStorage]  WITH CHECK ADD  CONSTRAINT [FK_GiftStorage_Gift] FOREIGN KEY([GiftId])
REFERENCES [dbo].[Gift] ([GiftId])
GO
ALTER TABLE [dbo].[GiftStorage] CHECK CONSTRAINT [FK_GiftStorage_Gift]
GO
ALTER TABLE [dbo].[HistoryImportGas]  WITH CHECK ADD  CONSTRAINT [FK_HistoryImportGas_Gas] FOREIGN KEY([GasId])
REFERENCES [dbo].[Gas] ([GasId])
GO
ALTER TABLE [dbo].[HistoryImportGas] CHECK CONSTRAINT [FK_HistoryImportGas_Gas]
GO
ALTER TABLE [dbo].[HistoryImportGift]  WITH CHECK ADD  CONSTRAINT [FK_HistoryImportGift_Gift] FOREIGN KEY([GiftId])
REFERENCES [dbo].[Gift] ([GiftId])
GO
ALTER TABLE [dbo].[HistoryImportGift] CHECK CONSTRAINT [FK_HistoryImportGift_Gift]
GO
ALTER TABLE [dbo].[HistoryImportProduct]  WITH CHECK ADD  CONSTRAINT [FK_HistoryImportProduct_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
GO
ALTER TABLE [dbo].[HistoryImportProduct] CHECK CONSTRAINT [FK_HistoryImportProduct_Product]
GO
ALTER TABLE [dbo].[LExpenditure]  WITH CHECK ADD  CONSTRAINT [FK_LExpenditure_TypeExpenditure] FOREIGN KEY([TypeExpenditureId])
REFERENCES [dbo].[TypeExpenditure] ([TypeExpenditureId])
GO
ALTER TABLE [dbo].[LExpenditure] CHECK CONSTRAINT [FK_LExpenditure_TypeExpenditure]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Customer]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Employee]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Gas] FOREIGN KEY([GasId])
REFERENCES [dbo].[Gas] ([GasId])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Gas]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Gift] FOREIGN KEY([GiftId])
REFERENCES [dbo].[Gift] ([GiftId])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Gift]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Product]
GO
ALTER TABLE [dbo].[ProductPrice]  WITH CHECK ADD  CONSTRAINT [FK_ProductPrice_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
GO
ALTER TABLE [dbo].[ProductPrice] CHECK CONSTRAINT [FK_ProductPrice_Product]
GO
ALTER TABLE [dbo].[ProductStorage]  WITH CHECK ADD  CONSTRAINT [FK_ProductStorage_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
GO
ALTER TABLE [dbo].[ProductStorage] CHECK CONSTRAINT [FK_ProductStorage_Product]
GO
ALTER TABLE [dbo].[Salary]  WITH CHECK ADD  CONSTRAINT [FK_Salary_Employee] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([EmployeeId])
GO
ALTER TABLE [dbo].[Salary] CHECK CONSTRAINT [FK_Salary_Employee]
GO
ALTER TABLE [dbo].[Salary]  WITH CHECK ADD  CONSTRAINT [FK_Salary_SalaryUnit] FOREIGN KEY([SalaryUnitId])
REFERENCES [dbo].[SalaryUnit] ([SalaryUnitId])
GO
ALTER TABLE [dbo].[Salary] CHECK CONSTRAINT [FK_Salary_SalaryUnit]
GO
