﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebGasManagement.Models
{
    public class WebGasManagementContext : DbContext
    {
        public WebGasManagementContext (DbContextOptions<WebGasManagementContext> options)
            : base(options)
        {
        }

        public DbSet<WebGasManagement.Models.Movie> Movie { get; set; }
    }
}
